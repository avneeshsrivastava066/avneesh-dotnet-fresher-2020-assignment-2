﻿using Castle.Components.DictionaryAdapter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuccessiveDotNetTest.Models
{
    public class DataViewModel
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Your First Name")]
        [MaxLength(50)]
        [RegularExpression("^[a-zA-Z]*$")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        [RegularExpression("^[a-zA-Z]*$")]
        public string LastName { get; set; }

        [EmailAddress]
        [Required]
        public string Email_ID { get; set; }

        [Required]
        [DisplayName("DOB"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime DOB { get; set; }

        [MaxLength(10)]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [Required]
        public int Experience { get; set; }

        [Required]
        public int CandidateId { get; set; }

        [Required]
        [DisplayName("Scheduled_Date"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime Scheduled_Date { get; set; }

        [Required]
        [DisplayName("TimeFrom")]
        [DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode = true)]
        public System.TimeSpan TimeFrom { get; set; }

        [Required]
        [DisplayName("TimeTo")]
        [DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode = true)]
        public System.TimeSpan TimeTo { get; set; }

        [Required]
        public string InterviewerName { get; set; }

        //public List<Candidate> Table1Records { get; set; }
        //public List<InterviewSchedule> Table2Records { get; set; }


    }
}