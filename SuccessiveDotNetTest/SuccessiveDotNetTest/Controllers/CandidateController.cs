﻿using SuccessiveDotNetTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuccessiveDotNetTest.Controllers
{
    public class CandidateController : Controller
    {
        Test2DBEntities entityObj = new Test2DBEntities();
       
        
        // GET: Candidate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ScheduleInterview()
        {
            var getResult = entityObj.Candidates.ToList();
            SelectList list = new SelectList(getResult, "Id", "FirstName");

            ViewBag.candidateList = list;
            return View();
        }


        [HttpPost]
        public ActionResult ScheduleInterview(DataViewModel ur)
        {
           
            if (ModelState.IsValid)
            {
                Candidate canObj = new Candidate();
                canObj.FirstName = ur.FirstName;
                canObj.LastName = ur.LastName;
                canObj.DOB = ur.DOB;
                canObj.Mobile = ur.Mobile;
                canObj.Experience = ur.Experience;
                canObj.Email_ID = ur.Email_ID;
                entityObj.Candidates.Add(canObj);
                entityObj.SaveChanges();


                InterviewSchedule scheduleObj = new InterviewSchedule();
                
                scheduleObj.Scheduled_Date = ur.Scheduled_Date;
                scheduleObj.TimeFrom = ur.TimeFrom;
                scheduleObj.TimeTo = ur.TimeTo;
                scheduleObj.InterviewerName = ur.InterviewerName;
                scheduleObj.CandidateId = canObj.Id;
                entityObj.InterviewSchedules.Add(scheduleObj);
                entityObj.SaveChanges();
                return RedirectToAction("ScheduledInterviewDetails");
            }

            return View(ur);
        }
        //public ActionResult ScheduleInterview()
        //{

        //    return View();
        //}

        public ActionResult ScheduledInterviewDetails()
        {
            List<Candidate> candidates = entityObj.Candidates.ToList();
            List<InterviewSchedule> interviewSchedules = entityObj.InterviewSchedules.ToList();

            var allRecord = from c in candidates
                                 join d in interviewSchedules on c.Id equals d.CandidateId into table1
                                 from d in table1.ToList()

                                 select new ViewModel
                                 {
                                     candidate = c,
                                     interviewSchedule = d,
                                   
                                 };
            return View(allRecord);
        }
        public ActionResult Edit(int id)
        {
            Candidate can = entityObj.Candidates.Find(id);
            if (can == null)
            {
                return HttpNotFound();
            }
            return View(ScheduledInterviewDetails());
        }
    }
}